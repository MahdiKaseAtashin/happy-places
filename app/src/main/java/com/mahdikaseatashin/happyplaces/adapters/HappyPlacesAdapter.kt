package com.mahdikaseatashin.happyplaces.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.mahdikaseatashin.happyplaces.R
import com.mahdikaseatashin.happyplaces.activities.AddHappyPlacesActivity
import com.mahdikaseatashin.happyplaces.activities.MainActivity
import com.mahdikaseatashin.happyplaces.databases.DataBaseHelperHappyPlaces
import com.mahdikaseatashin.happyplaces.models.HappyPlaceModel
import kotlinx.android.synthetic.main.activity_add_happy_places.*
import kotlinx.android.synthetic.main.item_happy_place.view.*

open class HappyPlacesAdapter(
    private val context: Context,
    private val list: ArrayList<HappyPlaceModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var onClickListener: OnClickListener? = null

    fun notifyEditItem(activity: Activity, position: Int, requestCode: Int, dir: Int) {
        if (MainActivity.RIGHT == dir) {
            val intent = Intent(context, AddHappyPlacesActivity::class.java)
            intent.putExtra(MainActivity.MODEL_TAG, list[position])
            activity.startActivityForResult(intent, requestCode)
        }else{
            val dbHandler = DataBaseHelperHappyPlaces(context)
            val result = dbHandler.deleteHappyPlace(list[position])
            if (result>0){
                Toast.makeText(context, "deleted", Toast.LENGTH_SHORT).show()
                list.removeAt(position)
            }
            notifyItemChanged(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VH(LayoutInflater.from(context).inflate(R.layout.item_happy_place, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = list[position]

        if (holder is VH) {
            holder.itemView.iv_place_item.setImageURI(Uri.parse(model.image))
            holder.itemView.txt_title.text = model.title
            holder.itemView.txt_description.text = model.description
            holder.itemView.setOnClickListener {
                if (onClickListener != null)
                    onClickListener!!.onClick(position, model)
            }
        }
    }

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun getItemCount(): Int = list.size

    interface OnClickListener {
        fun onClick(position: Int, model: HappyPlaceModel)
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView)

}
