package com.mahdikaseatashin.happyplaces.activities

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mahdikaseatashin.happyplaces.R
import com.mahdikaseatashin.happyplaces.models.HappyPlaceModel
import kotlinx.android.synthetic.main.activity_add_happy_places.*
import kotlinx.android.synthetic.main.activity_happy_places_detail.*

class HappyPlacesDetail : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_happy_places_detail)
        var position: Int? = null
        var model: HappyPlaceModel? = null
        if (intent.hasExtra(MainActivity.MODEL_TAG))
            model = intent.getParcelableExtra(MainActivity.MODEL_TAG) as HappyPlaceModel?

        if (model != null) {
            txt_title_detail.text = model.title
            txt_description_detail.text = model.description
            img_detail.setImageURI(Uri.parse(model.image))
        }

    }
}
