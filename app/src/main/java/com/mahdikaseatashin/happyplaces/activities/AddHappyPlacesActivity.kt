package com.mahdikaseatashin.happyplaces.activities

import android.Manifest
import android.R.attr
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.location.*
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.internal.ContextUtils.getActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.mahdikaseatashin.happyplaces.R
import com.mahdikaseatashin.happyplaces.databases.DataBaseHelperHappyPlaces
import com.mahdikaseatashin.happyplaces.models.HappyPlaceModel
import com.mahdikaseatashin.happyplaces.utils.GetAddressFromLatLng
import kotlinx.android.synthetic.main.activity_add_happy_places.*
import kotlinx.android.synthetic.main.activity_add_happy_places.view.*
import kotlinx.android.synthetic.main.activity_happy_places_detail.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class AddHappyPlacesActivity : AppCompatActivity(), View.OnClickListener, LocationListener {

    private var placeModel: HappyPlaceModel? = null
    private var position: Int? = null
    private lateinit var locationManager: LocationManager
    private var saveImageInInternalStorage: Uri? = null
    private var mLatitude: Double = 0.0
    private var mLongitude: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_happy_places)

        if (!Places.isInitialized()) {
            Places.initialize(this, resources.getString(R.string.google_maps_api_key))
        }

        if (intent.hasExtra(MainActivity.MODEL_TAG))
            placeModel = intent.getParcelableExtra(MainActivity.MODEL_TAG)

        initializeToolbar()
        if (placeModel != null) {
            supportActionBar?.title = "Edit Happy Place ${position?.plus(1)}"
            edt_title_add.setText(placeModel!!.title)
            edt_location_add.setText(placeModel!!.location)
            edt_date_add.setText(placeModel!!.date)
            edt_description_add.setText(placeModel!!.description)
            selected_image.setImageURI(Uri.parse(placeModel!!.image))
            saveImageInInternalStorage = Uri.parse(placeModel!!.image)
            save_happy_place.text = "Update"

        }
        edt_location_add.setOnClickListener(this)
        save_happy_place.setOnClickListener(this)
        edt_date_add.setOnClickListener(this)
        btn_add_image.setOnClickListener(this)
        btn_set_current_location.setOnClickListener(this)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

    private fun initializeToolbar() {
        setSupportActionBar(add_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        add_toolbar.setOnClickListener { }
    }

    @SuppressLint("RestrictedApi")
    override fun onClick(v: View?) {
        when (v?.id) {
 /*           edt_location_add.id -> {
                try {
                    if (getActivity(this) != null) {
                        val fields = Arrays.asList(
                            Place.Field.ID,
                            Place.Field.NAME,
                            Place.Field.LAT_LNG,
                            Place.Field.ADDRESS
                        )
                        val intent = Autocomplete.IntentBuilder(
                            AutocompleteActivityMode.FULLSCREEN, fields
                        )
                            .build(getActivity(this)!!)
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
                    }

                } catch (e: Exception) {
                    Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            }*/

            edt_date_add.id -> {
                initializeDatePicker()
            }

            btn_set_current_location.id -> {
                setCurrentLocation()
            }

            btn_add_image.id -> {
                val items = arrayOf("Capture Image", "Choose From Gallery")
                MaterialAlertDialogBuilder(this)
                    .setTitle("Add Image")
                    .setItems(items) { _, which ->
                        if (which == 0) {
                            captureImage()
                        } else if (which == 1) {
                            chooseImageFromGallery()
                        }
                    }
                    .show()
            }

            save_happy_place.id -> {
                when {
                    edt_title_add.text.isNullOrEmpty() -> {
                        edt_title_add.error = "Can not be empty"
                    }
                    edt_description_add.text.isNullOrEmpty() -> {
                        edt_description_add.error = "Can not be empty"
                    }
                    edt_date_add.text.isNullOrEmpty() -> {
                        edt_date_add.error = "Can not be empty"
                    }
                    saveImageInInternalStorage == null -> {
                        Toast.makeText(this, "Select an image", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        val happyPlaceModel = HappyPlaceModel(
                            if (placeModel == null) 0 else placeModel!!.id,
                            edt_title_add.text.toString(),
                            edt_description_add.text.toString(),
                            edt_date_add.text.toString(),
                            edt_location_add.text.toString(),
                            saveImageInInternalStorage.toString(),
                            mLatitude,
                            mLongitude
                        )
                        val dbHandler = DataBaseHelperHappyPlaces(this)
                        if (placeModel == null) {
                            val addHappyPlaceResult = dbHandler.addHappyPlace(happyPlaceModel)
                            if (addHappyPlaceResult > 0) {
                                setResult(Activity.RESULT_OK)
                                Toast.makeText(this, "saved successfully", Toast.LENGTH_SHORT)
                                    .show()
                                finish()
                            }
                        } else {
                            val updateHappyPlaceResult = dbHandler.updateHappyPlace(happyPlaceModel)
                            if (updateHappyPlaceResult > 0) {
                                setResult(Activity.RESULT_OK)
                                Toast.makeText(this, "edited successfully", Toast.LENGTH_SHORT)
                                    .show()
                                finish()
                            }
                        }


                    }
                }
            }
        }
    }

    private fun captureImage() {
        Dexter.withContext(this)
            .withPermission(
                Manifest.permission.CAMERA,
            ).withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(cameraIntent, CAPTURE_PHOTO_CODE)
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    if (p0?.isPermanentlyDenied == true) {
                        AlertDialog.Builder(this@AddHappyPlacesActivity)
                            .setTitle("Permission Denied")
                            .setMessage("We need you to allow us to access camera for this feature\nfor this thing to happen go to setting and give us the permission")
                            .setPositiveButton("Setting") { _, _ ->
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri: Uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivity(intent)
                            }
                            .setNegativeButton("Cancel") { dialog, _ ->
                                dialog.dismiss()
                            }.show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

            }).check()
    }

    private fun chooseImageFromGallery() {
        Dexter.withContext(this)
            .withPermission(
                Manifest.permission.READ_EXTERNAL_STORAGE,
            ).withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    val pickPhoto = Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    )
                    startActivityForResult(pickPhoto, PICK_PHOTO_CODE)
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    if (p0?.isPermanentlyDenied == true) {
                        AlertDialog.Builder(this@AddHappyPlacesActivity)
                            .setTitle("Permission Denied")
                            .setMessage("We need you to allow us to read gallery for this feature\nfor this thing to happen go to setting and give us the permission")
                            .setPositiveButton("Setting") { _, _ ->
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri: Uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivity(intent)
                            }
                            .setNegativeButton("Cancel") { dialog, _ ->
                                dialog.dismiss()
                            }.show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

            }).check()
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    @SuppressLint("RestrictedApi")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            when (requestCode) {
                CAPTURE_PHOTO_CODE -> {
                    if (resultCode == RESULT_OK && data != null) {
                        val capturedImage: Bitmap = data.extras!!.get("data") as Bitmap
                        val cw = ContextWrapper(applicationContext)
                        val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
                        val myPath = File(directory, "${UUID.randomUUID()}.jpg")
                        var fos: FileOutputStream? = null
                        try {
                            fos = FileOutputStream(myPath)
                            // Use the compress method on the BitMap object to write image to the OutputStream
                            capturedImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        } finally {
                            try {
                                fos?.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                        saveImageInInternalStorage = Uri.parse(filesDir.absolutePath)
                        selected_image.setImageBitmap(capturedImage)
                    }
                }

                PICK_PHOTO_CODE -> {
                    if (resultCode == RESULT_OK && data != null) {
                        var bm: Bitmap? = null
                        try {
                            bm = MediaStore.Images.Media.getBitmap(
                                applicationContext.contentResolver,
                                data.data
                            )
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        saveImageInInternalStorage = bm?.let { getImageUri(this, it) }
                        selected_image.setImageBitmap(bm)
                    }
                }

              /*  PLACE_AUTOCOMPLETE_REQUEST_CODE -> {
                    if (resultCode === RESULT_OK) {
                        Log.e("TAG", "onActivityResult: ok")
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        if (place.latLng != null) {
                            // reverse geoCoding to get Street Address, city,state and postal code
                            val geocoder = Geocoder(getActivity(this), Locale.getDefault())
                            try {
                                println("------addressList-----" + place.address + "             " + place.name)
                                val addressList = geocoder.getFromLocation(
                                    place.latLng!!.latitude, place.latLng!!.longitude, 1
                                )
                                println("------addressList-----$addressList")
                                if (addressList != null && addressList.size > 0) {
                                    val address = addressList[0]
                                    println("------address-----$address")
//                                    addressEd.setText(address.getAddressLine(0))
                                    Log.e(
                                        "Address",
                                        "onActivityResult: ${address.getAddressLine(0)}"
                                    )
                                    var featureName = ""
                                    if (address.featureName != null) {
                                        featureName = address.featureName
                                    }
                                    var throughFare = ""
                                    if (address.thoroughfare != null) {
                                        throughFare = address.thoroughfare
                                    }
                                    val streetAddress = "$featureName $throughFare"
//                                    streetAddressEd.setText(streetAddress)
                                    if (address.locality != null) {
//                                        cityEd.setText(address.locality)
                                    } else {
//                                        callGeoCodeAPI(place.latLng!!.latitude.toString() + "," + place.latLng!!.longitude)
                                    }
//                                    stateEd.setText(address.adminArea)
//                                    postCodeEd.setText(address.postalCode)
//                                    countryEd.setText(address.countryName)
                                }
                            } catch (e: IOException) {
                                Log.e("TAG", "Unable connect to Geocoder", e)
                            }
                        }
                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                        Log.e("TAG", "onActivityResult: Error")
                        val status: Status = Autocomplete.getStatusFromIntent(data!!)
                        if (getActivity(this) != null) {
                            Log.e("TAG", "Error : ${status.statusMessage}")
                        }
                    } else if (resultCode == RESULT_CANCELED) {
                        Log.e("TAG", "onActivityResult: canceled")
                        // The user canceled the operation.
                    }
                }*/
            }
        }
    }

    private fun setCurrentLocation() {
        Dexter.withContext(this)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionGranted(report: PermissionGrantedResponse?) {
                    locationManager =
                        getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        5000,
                        5f,
                        this@AddHappyPlacesActivity
                    )
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    if (response?.isPermanentlyDenied == true) {
                        AlertDialog.Builder(this@AddHappyPlacesActivity)
                            .setTitle("Permission Denied")
                            .setMessage("We need you to allow us to use gps for this feature for this thing to happen go to setting and give us the permission")
                            .setPositiveButton("Setting") { _, _ ->
                                val intent =
                                    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri: Uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivity(intent)
                            }
                            .setNegativeButton("Cancel") { dialog, _ ->
                                dialog.dismiss()
                            }.show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

            })
            .check()
    }

    override fun onLocationChanged(location: Location) {

        Toast.makeText(this, "finding location", Toast.LENGTH_SHORT).show()
        Toast.makeText(this, "lat : ${location.latitude}", Toast.LENGTH_SHORT).show()
        Toast.makeText(this, "lng : ${location.longitude}", Toast.LENGTH_SHORT).show()


        val addressTask = GetAddressFromLatLng(this,location.latitude,location.longitude)
        addressTask.setAddressListener(object : GetAddressFromLatLng.AddressListener {
            override fun onAddressFound(address: String?) {
                Log.e("TAG", "onAddressFound: $address")
                edt_location_add.setText(address)
            }

            override fun onError() {
                Log.e("TAG", "onError: get address error")
            }
        })
        addressTask.getAddress()
    }

    private fun initializeDatePicker() {
        val c = Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)
        val d = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in TextView
            edt_date_add.setText("$year/${m + 1}/$dayOfMonth")
        }, y, m, d)
        dpd.datePicker.maxDate = c.timeInMillis
        dpd.show()
    }

    companion object {
        const val PICK_PHOTO_CODE = 1
        const val CAPTURE_PHOTO_CODE = 0
        const val PLACE_AUTOCOMPLETE_REQUEST_CODE = 2
    }
}
